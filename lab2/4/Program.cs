﻿using System;

namespace _4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите номер карты от 6 до 14");
            int k = Convert.ToInt32(Console.ReadLine());
            Console.Write("Значение карты: ");
            switch (k)
            {
                case 6: Console.WriteLine("Шестёрка"); break;
                case 7: Console.WriteLine("Семёрка"); break;
                case 8: Console.WriteLine("Восьмёрка"); break;
                case 9: Console.WriteLine("Девятка"); break;
                case 10: Console.WriteLine("Десятка"); break;
                case 11: Console.WriteLine("Валет"); break;
                case 12: Console.WriteLine("Дама"); break;
                case 13: Console.WriteLine("Король"); break;
                case 14: Console.WriteLine("Туз"); break;
                default: Console.WriteLine("Введено значение вне диапазона"); break;
            }
            Console.ReadKey();
        }
    }
}
