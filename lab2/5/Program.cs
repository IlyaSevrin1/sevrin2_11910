﻿using System;

namespace _5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите значение масти от 1 до 4: ");
            int m = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введите значение карты от 6 до 14");
            int k = Convert.ToInt32(Console.ReadLine());         
            if ((k <= 14) && ((k >= 6)) && (m >= 1) && (m <= 4))
            {
                Console.Write("Значение карты и её масть - ");
                switch (k)
                {
                    case 6: Console.Write("Шестёрка"); break;
                    case 7: Console.Write("Семёрка"); break;
                    case 8: Console.Write("Восьмёрка"); break;
                    case 9: Console.Write("Девятка"); break;
                    case 10: Console.Write("Десятка"); break;
                    case 11: Console.Write("Валет"); break;
                    case 12: Console.Write("Дама"); break;
                    case 13: Console.Write("Король"); break;
                    case 14: Console.Write("Туз"); break;
                }
                switch (m)
                {
                    case 1: Console.WriteLine(" пик"); break;
                    case 2: Console.WriteLine(" треф"); break;
                    case 3: Console.WriteLine(" бубен"); break;
                    case 4: Console.WriteLine(" черв"); break;
                }
                    
            }
            else
            {
                Console.WriteLine("Одно из значение не соответствует диапазону");
            }
            Console.ReadKey();
        } 
    }
}
