﻿using System;

namespace _6
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите признак транспортного средства(а - автомобиль, в - велосипед, м - мотоцикл, с - самолёт, п - поезд)");
            string p = Convert.ToString(Console.ReadLine());
            Console.Write("Максимальная скорость: ");
            switch (p)
            {
                case "а": Console.Write("120 км/ч"); break;
                case "в": Console.Write("40 км/ч"); break;
                case "м": Console.Write("100 км/ч"); break;
                case "с": Console.Write("800 км/ч"); break;
                case "п": Console.Write("400 км/ч"); break;
                default: Console.WriteLine("Введено неизвестное значение"); break;
            }
            Console.ReadKey();
        }
    }
}
