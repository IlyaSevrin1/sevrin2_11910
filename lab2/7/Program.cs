﻿using System;

namespace _7
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите признак фигуры (к - круг, п - прямоугольник, т - треугольник)");
            string f = Console.ReadLine();
            switch (f)
            {
                case "к": Console.WriteLine("Введите радиус круга:");
                    double r = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine("Площадь круга равна: " + (3 + 0.14 * Math.Pow(r, 2)));
                    Console.WriteLine("Периметр круга равен: " + (2 * 3 + 0.14 * r)); break;
                case "п": Console.WriteLine("Введите стороны а и в прямоугольника");
                    double a = Convert.ToDouble(Console.ReadLine());
                    double b = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine("Площадь прямоугольника: " + (a * b));
                    Console.WriteLine("Периметр прямоугольника: " + (a * 2 + b * 2)); break;
                case "т": Console.WriteLine("Введите 2 стороны треугольника");
                    double side1 = Convert.ToDouble(Console.ReadLine());
                    double side2 = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine("Введите основание треугольника");
                    double side3 = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine("Введите высоту треугольника");
                    double h = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine("Площадь треугольника: " + (h * side3));
                    Console.WriteLine("Периметр треугольника: " + (side1 + side2 + side3)); break;
                default: Console.WriteLine("Введено неизвестное значение."); break;

            }   
        }
    }
}
