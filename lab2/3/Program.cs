﻿using System;

namespace _3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите значение масти m от 1 до 4");
            int m = Convert.ToInt32(Console.ReadLine());
            switch (m)
            {
                case 1: Console.WriteLine("Масть - пики"); break;
                case 2: Console.WriteLine("Масть - трефы"); break;
                case 3: Console.WriteLine("Масть - бубны"); break;
                case 4: Console.WriteLine("Масть - червы"); break;
                default: Console.WriteLine("Введено значение вне диапазона"); break;
            }
            Console.ReadKey();
        }
    }
}
