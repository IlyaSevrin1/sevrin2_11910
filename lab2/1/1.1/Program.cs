﻿using System;

namespace lab2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите координату точки на оси х:");
            double x = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите координату точки на оси у:");
            double y = Convert.ToDouble(Console.ReadLine());
            double c = Math.Sqrt(Math.Pow(x, 2) + Math.Pow(y, 2));
            double c2 = 9;
            if (c < c2)
            {
                if ((x < 9) && (y < 9) && (y > -9) && (x > 0))
                {
                    Console.WriteLine("Точка лежит в заштрихованной области");
                }
                else
                {
                    Console.WriteLine("Точка лежит вне заштрихованной области");
                }
            }
            else if (c > c2)
            {
                Console.WriteLine("Точка лежит вне заштрихованной области");
            }
            else if (c == c2)
            {
                if (x>=0)
                {
                    Console.WriteLine("Точка лежит на границе");
                }
                else
                {
                    Console.WriteLine("Точка лежит вне заштрихованной области");
                }
            }
            
        }
    }
}
