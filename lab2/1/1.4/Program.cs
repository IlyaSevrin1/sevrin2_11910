﻿using System;

namespace _1._5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите координаты на оси х:");
            double x = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите координаты на оси у");
            double y = Convert.ToDouble(Console.ReadLine());
            double c = Math.Sqrt(Math.Pow(x, 2) + Math.Pow(y, 2));
            int r = 25;
            double b = -Math.Abs(x);
            if ((y > 0) || (b < y) || c > r)
            {
                Console.WriteLine("Точка лежит вне заштрихованной области");
            }
            else if ((c == r) && (y <= b) || ((y == b) && (c <= -r)))
            {
                Console.WriteLine("Точка лежит на границе");
            }
            else
            {
                Console.WriteLine("Точка лежит в заштрихованной области");
            }
            Console.ReadKey();
        }
    }
}
