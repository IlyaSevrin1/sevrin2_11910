﻿using System;

namespace _1._1._2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите координату точки на оси х:");
            double x = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите координату точки на оси у:");
            double y = Convert.ToDouble(Console.ReadLine());
            double C = Math.Sqrt(Math.Pow(x, 2) + Math.Pow(y, 2));
            double Clarge = 8;
            double Csmall = 3;
            if ((C < Csmall) || (x > 0))
            {
                Console.WriteLine("Точка лежит вне заштрихованной области");
            }
            else if ((C < Clarge) && (C > Csmall))
            {
                if (x < 0)
                {
                    Console.WriteLine("Точка лежит в заштрихованной области");
                }
                else if (x == 0)
                {
                    Console.WriteLine("Точка лежит на границе");
                }
            }
            else if ((C > Clarge) || (x>0))
            {
                Console.WriteLine("Точка лежит вне заштрихованной области");
            }
            else if ((C == Clarge) || (C == Csmall))
            {
                Console.WriteLine("Точка лежит на границе");
            }
        }
    }
}
