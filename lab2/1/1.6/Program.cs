﻿using System;

namespace _1._6
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите координату х:");
            double x = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите координату у");
            double y = Convert.ToDouble(Console.ReadLine());
            if ((y<40) && (y > -40))
            {
                if ((x<40) && (x > -40))
                {
                    Console.WriteLine("Точка лежит вне заштрихованной области");
                }
            }
            else if ((((y==40) || (y==-40)) && (x<=40) && (x>=-40)) || ((x==40) || (x==-40) && (y<=40) && (y>=-40))) 
            {
                Console.WriteLine("Точка лежит на границе");
            }
            else 
            {
                Console.WriteLine("Точка лежит в заштрихованной области");
            }
        }
    }
}
