﻿using System;

namespace _1._1._3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите координату точки на оси х:");
            double x = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите координату точки на оси у:");
            double y = Convert.ToDouble(Console.ReadLine());
            double c = Math.Sqrt(Math.Pow(x, 2) + Math.Pow(y, 2));
            double Clarge = 25;
            double Csmall = 15;
            if ((c > Clarge) ||( c < Csmall))
            {
                Console.WriteLine("Точка лежит в заштрихованной области");
            }
            else if ((c < Clarge) && (c > Csmall))
            {
                Console.WriteLine("Точка лежит вне заштрихованной области");
            }
            else if ((c == Clarge) || (c == Csmall))
            {
                Console.WriteLine("Точка лежит на границе");
            }
        }
    }
}
