﻿using System;

namespace _9._1
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 100;
            for (i = 100; i < 1000; i++)
            {
                if ((i % 10) == (i / 10 % 10) || (i % 10) == (i / 100 % 10) || (i / 10 % 10) == (i /100 % 10))
                {
                    Console.WriteLine(i);
                }
            }
        }
    }
}
