﻿using System;

namespace _3._1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите число А");
            double A = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введите число В");
            double B = Convert.ToInt32(Console.ReadLine());
            for (double i = B; i>=A; i--)
            {
                Console.WriteLine(Math.Pow(i, 3));
            }
        }
    }
}
