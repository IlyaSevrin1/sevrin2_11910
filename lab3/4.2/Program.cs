﻿using System;

namespace _4._2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите число А");
            double A = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите число В");
            double B = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите число Х");
            double X = Convert.ToDouble(Console.ReadLine());
            double i = A;
            while (i <= B)
            {
                if (i % 10 == X)
                {
                    Console.WriteLine(i);
                }
                i++;
            }
        }
    }
}
