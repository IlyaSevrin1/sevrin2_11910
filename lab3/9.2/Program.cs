﻿using System;

namespace _9._2
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 100;
            while (i < 1000)
            { 
            if ((i % 10) == (i / 10 % 10) || (i % 10) == (i / 100 % 10) || (i / 10 % 10) == (i / 100 % 10))
            {
                Console.WriteLine(i);
            }
                i++;
            }
        }
    }
}
