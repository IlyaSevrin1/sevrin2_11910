﻿using System;

namespace _8._2
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 9;
            while (i < 100)
            {
                if ((i % 10) > (i / 10))
                {
                    if ((i % 10) - (i / 10) <= 1)
                    {
                        Console.WriteLine(i);
                    }
                }
                else
                {
                    if ((i / 10) - (i % 10) <= 1)
                    {
                        Console.WriteLine(i);
                    }
                }
                i++;
            }
        }
    }
}
