﻿using System;

namespace _6._1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите число А");
            double A = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите число В");
            double B = Convert.ToDouble(Console.ReadLine());
            for (double i = A; i <= B; i++)
            {
                if (i % 3 == 0)
                {
                    Console.WriteLine(i);
                }
            }
        }
    }
}
