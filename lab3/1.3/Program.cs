﻿using System;

namespace _1._3
{
    class Program
    {
        static void Main(string[] args)
        {
            double n = 60;
            do
            {
                if (n % 2 == 0)
                {
                    Console.WriteLine(n);
                }
                n--;
            } while (n >= 10);
            Console.ReadKey();
        }
    }
}
