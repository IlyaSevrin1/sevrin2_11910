﻿using System;

namespace _9._3
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 10;
            do
            {
                if ((i % 10) == (i / 10 % 10) || (i % 10) == (i / 100 % 10) || (i / 10 % 10) == (i / 100 % 10))
                {
                    Console.WriteLine(i);
                }
                i++;
            } while (i < 1000);
        }
    }
}
