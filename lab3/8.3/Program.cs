﻿using System;

namespace _8._3
{
    class Program
    {
        static void Main(string[] args)
        {
            double i = 10;
            do
            {
                if ((i % 10) > (i / 10))
                {
                    if ((i % 10) - (i / 10) <= 1)
                    {
                        Console.WriteLine(i);
                    }
                }
                else
                {
                    if ((i / 10) - (i % 10) <= 1)
                    {
                        Console.WriteLine(i);
                    }
                }
                i++;
            } while (i < 100);
        }
    }
}
