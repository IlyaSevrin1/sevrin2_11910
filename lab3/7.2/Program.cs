﻿using System;

namespace _7._2
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 10;
            while(i < 100)
            {
                if((i%10) != (i / 10))
                {
                    Console.WriteLine(i);
                }
                i++;
            }
            Console.ReadKey();
        }
    }
}
