﻿using System;

namespace _3._2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите число А");
            double A = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введите число В");
            double B = Convert.ToInt32(Console.ReadLine());
            double i = B;
            while (i >= A)
            {
                Console.WriteLine(Math.Pow(i, 3));
                i--;
            }
        }
    }
}
