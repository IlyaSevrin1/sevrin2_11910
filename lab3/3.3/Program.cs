﻿using System;

namespace _3._3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите число А");
            double A = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введите число В");
            double B = Convert.ToInt32(Console.ReadLine());
            double i = B;
            do
            {
                Console.WriteLine(Math.Pow(i,3));
                i--;
            } while (i >= A);
            Console.ReadKey();
        }
    }
}
