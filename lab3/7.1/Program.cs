﻿using System;

namespace _7._1
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 10; i<100; i++)
            {
                if ((i % 10) != (i /10 % 10))
                {
                    Console.WriteLine(i);
                }
            }
            Console.ReadKey();
        }
    }
}
