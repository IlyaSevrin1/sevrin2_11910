﻿using System;

namespace _7._3
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 10;
            do
            {
                if ((i % 10) != (i / 10))
                {
                    Console.WriteLine(i);
                }
                i++;
            } while (i < 100);
            Console.ReadKey();
        }
    }
}
