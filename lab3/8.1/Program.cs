﻿using System;

namespace _8._1
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 9; i < 100; i++)
            {
                if ((i % 10) > (i / 10))
                {
                    if ((i % 10) - (i / 10) <= 1)
                    {
                        Console.WriteLine(i);
                    }
                }
                else
                {
                    if ((i / 10) - (i % 10) <= 1)
                    {
                        Console.WriteLine(i);
                    }
                }
            }
        }
    }
}
