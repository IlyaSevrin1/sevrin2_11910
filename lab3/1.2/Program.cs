﻿using System;

namespace _1._2
{
    class Program
    {
        static void Main(string[] args)
        {
            double n = 60;
            while (n >= 10)
            {
                if (n % 2 == 0)
                {
                    Console.WriteLine(n);
                }
                n--;
            }
            Console.ReadKey();
        }
    }
}
