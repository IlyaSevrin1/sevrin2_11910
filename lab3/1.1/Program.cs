﻿using System;

namespace lab3
{
    class Program
    {
        static void Main(string[] args)
        {
            double i;
            for (i = 60; i >= 10; i--)
            {
                if (i % 2 == 0)
                {
                    Console.WriteLine(i);
                }
            }
            Console.ReadKey();
        }
    }
}