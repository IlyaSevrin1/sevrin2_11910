﻿using System;

namespace lab5
{
    class Program
    {
        static double met(double x, double y)
        {
            return (x + y) / 2;
        }
        static double met(double x, double y, double z)
        {
            return (x + y + z) / 3;
        }
        static double met(double x, double y, double z, double e)
        {
            return (x + y + z + e) / 4;
        }
        static double met(int a)
        {
            double b = 0.0;
            Console.WriteLine("Введите числа:");
            double i = a;
            while (a > 0)
            {
                b = b + Convert.ToDouble(Console.ReadLine());
                a++;
            }
            Console.WriteLine("Среднее арифметическое:");
            return b / i;
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Введите числа a, b, c, d:");
            double a = Convert.ToDouble(Console.ReadLine());
            double b = Convert.ToDouble(Console.ReadLine());
            double c = Convert.ToDouble(Console.ReadLine());
            double d = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Среднее арифметическое чисел a и b:");
            Console.WriteLine(met(a, b));
            Console.WriteLine("Среднее арифметическое чисед a, b, c:");
            Console.WriteLine(met(a, b, c));
            Console.WriteLine("Среднее арифметическое чисел a, b, c, d:");
            Console.WriteLine(met(a, b, c, d));
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Введите кол-во:");
            int r = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(met(r));

        }
    }
}
