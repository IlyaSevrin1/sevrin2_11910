﻿using System;

namespace lab5._3
{
    class Program
    {
        static string met1(double a)
        {
            int n = 6;
            int i = 0;
            while (n > 3)
            {
                i += (int)a % 10;
                a = a / 10;
                n--;
            }
            int g = 0;
            while (n > 0)
            {
                g += (int)a % 10;
                a = a / 10;
                n--;
            }
            if (i == g)
            {
                return "Билет счастливый";
            }
            else
            {
                return "Билет не счастливый";
            }
        }
        static string met1(double a, double b, double c, double d, double f, double g)
        {
            if ((a + b + c == d + f + g))
            {
                return "Билет счастливый";
                }
            else
            {
                return "Билет не счастливый";
            }
        } 
        static string met1(double a1, double a2)
        {
            int i = 0;
            int n = 6;
            int g = 0;
            while (n > 3)
            {
                i += (int)a1 % 10;
                a1 = a1 / 10;
                n--;
            }
            while(n > 0)
            {
                g += (int)a2 % 10;
                a2 = a2 / 10;
                n--;
            }
            if (g == i)
            {
                return "Билет счастливый";
            }
            else
            {
                return "Билет не счастливый";
            }
            
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Пункт 1");
            Console.WriteLine("-------------------");
            Console.WriteLine("Введите номер билета");
            double ticket1 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine(met1(ticket1));
            Console.WriteLine("Пункт 2");
            Console.WriteLine("Введите номер билета по одной цифре");
            double a1 = Convert.ToDouble(Console.ReadLine());
            double a2 = Convert.ToDouble(Console.ReadLine());
            double a3 = Convert.ToDouble(Console.ReadLine());
            double a4 = Convert.ToDouble(Console.ReadLine());
            double a5 = Convert.ToDouble(Console.ReadLine());
            double a6 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine(met1(a1, a2, a3, a4, a5, a6));
            Console.WriteLine("Пункт 3");
            Console.WriteLine("Введите номер билета по 3 числа");
            double b1 = Convert.ToDouble(Console.ReadLine());
            double b2 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine(met1(b1, b2));
        }
    }
}
