﻿using System;

namespace Lab2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите число а:");
            int a = Convert.ToInt32(Console.ReadLine());
            double b = a % 2;
            if (b == 0)
            {
                Console.WriteLine("Число а чётное");
            }
            else
            {
                Console.WriteLine("Число а нечётное");
            }
            Console.ReadKey();
        }
    }
}
