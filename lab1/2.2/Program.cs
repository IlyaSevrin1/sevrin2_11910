﻿using System;

namespace _1._2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите число М:");
            int M = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введите число N:");
            int N = Convert.ToInt32(Console.ReadLine());
            double B = M / N;
            if (M % N == 0)
            {
                Console.WriteLine("Частное между M и N:" + B);
            }
            else
            {
                Console.WriteLine("М на N нацело не делится");
            }
            Console.ReadKey();

        }
    }
}
