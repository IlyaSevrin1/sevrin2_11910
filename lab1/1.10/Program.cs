﻿using System;

namespace _1._10
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите первый член прогрессии:");
            double a1 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите шаг прогрессии:");
            double d = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите число членов прогрессии:");
            double n = Convert.ToDouble(Console.ReadLine());
            double Sn = (2 * a1 + (n - 1) * d) / 2 * n;
            Console.WriteLine("Сумма членов проогрессии равна: " + Sn);
            Console.ReadKey();
               
        }
    }
}
