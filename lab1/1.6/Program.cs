﻿using System;

namespace _1._6
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите координаты x1, x2, x3");
            double x1 = Convert.ToDouble(Console.ReadLine());
            double x2 = Convert.ToDouble(Console.ReadLine());
            double x3 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите координаты y1, y2, y3");
            double y1 = Convert.ToDouble(Console.ReadLine());
            double y2 = Convert.ToDouble(Console.ReadLine());
            double y3 = Convert.ToDouble(Console.ReadLine());
            double side1 = Math.Sqrt(Math.Abs(Math.Pow((x1 - x2), 2)) + Math.Abs(Math.Pow((y1 - y2), 2)));
            double side2 = Math.Sqrt(Math.Abs(Math.Pow((x2 - x3), 2)) + Math.Abs(Math.Pow((y2 - y3), 2)));
            double side3 = Math.Sqrt(Math.Abs(Math.Pow((x1 - x3), 2)) + Math.Abs(Math.Pow((y1 - y3), 2)));
            double P = side1 + side2 + side3;
            Console.WriteLine("Периметр треугольника равен: " + P);
            Console.ReadKey();


        }
    }
}
