﻿using System;

namespace _1._4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите значение первого катета");
            double a = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите значение второго катета");
            double b = Convert.ToDouble(Console.ReadLine());
            double c = Math.Sqrt(Math.Pow(a, 2) + Math.Pow(b, 2)) + a + b;
            Console.WriteLine("Периметр прямоугольного треугольника равен " + c);
            Console.ReadKey();

        }
    }
}
