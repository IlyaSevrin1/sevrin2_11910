﻿using System;

namespace _2._4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите двузначное число:");
            int a = Convert.ToInt32(Console.ReadLine());
            int b = (int) a / 10;
            double c = a % 10;
            if (b > c)
            {
                Console.WriteLine("Первая цифра числа больше второй");
            }
            else if (b < c)
            {
                Console.WriteLine("Вторая цифра числа больше первой");
            }
            else
            {
                Console.WriteLine("Первая и вторая цифры числа совпадают");
            }
            Console.ReadKey();
        }
    }
}
