﻿using System;

namespace _1._5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите объём куба:");
            double V = Convert.ToDouble(Console.ReadLine());
            double a = Math.Pow(V, 1.0 / 3.0);
            Console.WriteLine("Ребро куба равно " + a);
            Console.ReadKey();

        }
    }
}
