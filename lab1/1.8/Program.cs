﻿using System;

namespace _1._8
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите значение первого основания равнобедренной трапеции:");
            double a = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите значение второго основания равнобедренной трапеции:");
            double b = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите значение угла при большем основании:");
            double corner = Convert.ToDouble(Console.ReadLine());
            double side = (a - b) / (2 * Math.Cos(corner));
            double S = ((a + b) / 2) * Math.Sqrt(Math.Pow(side, 2) - ((Math.Pow((a - b), 2)) / 4));
            Console.WriteLine("Площадь равнобедренной трапеции равна: " + S);
            Console.ReadKey();


        }
    }
}
