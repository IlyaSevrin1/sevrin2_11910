﻿using System;

namespace _1._2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите первое число:");
            double a1 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите второе число:");
            double a2 = Convert.ToDouble(Console.ReadLine());
            double cube1 = Math.Pow(a1, 3);
            double cube2 = Math.Pow(a2, 3);
            double aver = (cube1 + cube2) / 2;
            Console.WriteLine("Среднее арифметическое кубов данных чисел равно:");
            Console.WriteLine(aver);
            Console.ReadKey();
        }
    }
}
