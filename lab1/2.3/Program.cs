﻿using System;

namespace _2._3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите целое число:");
            int a = Convert.ToInt32(Console.ReadLine());
            double b = a % 10;
            if (b == 7) 
            {
                Console.WriteLine("Число оканчивается на 7");
            }
            else
            {
                Console.WriteLine("Число не оканчивается на 7");
            }
            Console.ReadKey();

        }
    }
}
