﻿using System;

namespace _2._5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите двузначное число:");
            int a = Convert.ToInt32(Console.ReadLine());
            int b = (int)a / 10;
            int c = (int)a % 10;
            if (b == c)
            {
                Console.WriteLine("Цифры числа совпадают");
            }
            else
            {
                Console.WriteLine("Цифры числа не совпадают");
            }
            Console.ReadKey();
        }
    }
}
