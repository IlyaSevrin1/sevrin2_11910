﻿using System;

namespace _1._9
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите значение стороны треугольника:");
            double a = Convert.ToDouble(Console.ReadLine());
            double P = (a + a + a) / 2;
            double r = Math.Sqrt(Math.Pow((P - a), 3) / P);
            Console.WriteLine("Радиус вписанной окружности равен: " + r);
            Console.ReadKey();


        }
    }
}
