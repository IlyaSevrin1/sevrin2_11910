﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите периметр треугольника:");
            double P = Convert.ToDouble(Console.ReadLine());
            double S = (Math.Sqrt(3)) / 4 * Math.Pow(P / 3, 2);
            Console.WriteLine("Площадь треугольника равна:");
            Console.WriteLine(S);
            Console.ReadKey();
        }
    }
}
