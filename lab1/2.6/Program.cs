﻿using System;

namespace _2._6
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите стороны треугольника:");
            double a = Convert.ToDouble(Console.ReadLine());
            double b = Convert.ToDouble(Console.ReadLine());
            double c = Convert.ToDouble(Console.ReadLine());
            if ((a + b > c) & (a + c > b) & (b + c > a))
            {
                Console.WriteLine("Такой треугольник существует");
            }
            else
            {
                Console.WriteLine("Такой треугольник не существует");
            }
            Console.ReadKey();
        }
    }
}
