﻿using System;
namespace Contr
{
    class Program
    {
        static void PrintMas(int[] a)
        {
            for(int i = 0; i < a.Length; i++)
            {
                Console.Write(a[i]);
                Console.Write(' ');
            }
            Console.WriteLine();
        }
        static int[] EnterMas(int[] a)
        {
            Random ran = new Random();
            for (int i = 0; i < a.Length; i++)
            {
                a[i] = ran.Next(30, 100);
            }
            return a;
        }
        static void MasOperation1(int[] a, int[] b)
        {
            int l = 0;
            Console.WriteLine("Кол-во выигранных матчей");
            for(int i = 0; i < a.Length; i++)
            {
                if (a[i] > b[i])
                {
                    l++;
                }
            }
            Console.WriteLine(l);
        }
        static void MasOperation2(int[] a, int[] b)
        {
            Console.WriteLine("Кол-во матчей, в которых была ничья");
            int l = 0;
            for(int i = 0; i < a.Length; i++)
            {
                if(a[i] == b[i])
                {
                    l++;
                }
            }
            Console.WriteLine(l);
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Вариант 2");
            Console.WriteLine("Задание 2");
            int[] a1 = new int[15];
            int[] a2 = new int[15];
            EnterMas(a1);
            Console.WriteLine("Заброшенные мячи:");
            PrintMas(a1);
            EnterMas(a2);
            Console.WriteLine("Пропущенные мячи:");
            PrintMas(a2);
            MasOperation1(a1, a2);
            MasOperation2(a1, a2);
            Console.ReadKey();
        }
    }
}
