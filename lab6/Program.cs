﻿using System;

namespace lab6
{
    class Program
    {
        static int[] MasOperation1(int[] n)
        {
            for(int i = 0; i < n.Length; i++)
            {
                n[i] = n[i] * 2; 
            }
            return n;
        }
        static int[] MasOperation2(int[] n)
        {
            for(int i = 0; i < n.Length; i++)
            {
                n[i] = n[i] / n[0];
            }
            return n;
        }
        static int[] MasOperation3(int[] n)
        {
            Console.WriteLine("Введите число А");
            int A = Convert.ToInt32(Console.ReadLine());
            for (int i = 0; i < n.Length; i++) 
            {
                n[i] = n[i] + A;
            }
            return n;
        }
        static int[] Enter(int[] n)
        {
            Random ran = new Random();
            for (int i = 0; i < n.Length; i++)
            {
                n[i] = ran.Next(50, 100);
            }
            return n;
        }
        static int[] Enter2(int[] n)
        {
            Random ran = new Random();
            for (int i = 0; i < n.Length; i++)
            {
                n[i] = ran.Next(-50, 50);
            }
            return n;
        }
        static void Print(int[] n, int b)
        {
            for(int i = 0; i < b; i++)
            Console.WriteLine(n[i]);
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Задание 1");
            Console.WriteLine("Вес 20 человек");
            int[] n = new int[20];
            Enter(n);
            Print(n, 20);
            Console.WriteLine("-------");
            Console.WriteLine("Задание 2");
            Console.WriteLine("Введите число элементов массива");
            int a = Convert.ToInt32(Console.ReadLine());
            n = new int[a];
            Enter(n);
            MasOperation1(n);
            Print(n, a);
            Console.WriteLine("Введите число элементов массива");
            int a1 = Convert.ToInt32(Console.ReadLine());
            n = new int[a];
            Enter(n);
            MasOperation2(n);
            Print(n, a1);
            Console.WriteLine("Введите число элементов массива");
            int a2 = Convert.ToInt32(Console.ReadLine());
            n = new int[a];
            Enter(n);
            MasOperation3(n);
            Print(n, a2);
            Console.WriteLine("Задание 3");
            int Sum = 0;
            Console.WriteLine("Введите число элементов массива");
            int a3 = Convert.ToInt32(Console.ReadLine());
            n = new int[a];
            Enter(n);
            for (int i = 0; i < n.Length; i++)
            {
                Sum = Sum + n[i];
            }
            if (Sum % 2 == 0)
            {
                Console.WriteLine("Yes");
            }
            else
            {
                Console.WriteLine("No");
            }
            double SumSquare = 0;
            for(int i = 0; i < n.Length; i++)
            {
                SumSquare = SumSquare + Math.Pow(n[i], 2);
            }
            if (SumSquare.ToString().Length == 5)
            {
                Console.WriteLine("Yes");
            }
            else
            {
                Console.WriteLine("No");
            }
            Console.WriteLine("Задиние 4");
            Console.WriteLine("Введите число элементов массива");
            int a4 = Convert.ToInt32(Console.ReadLine());
            n = new int[a4];
            Enter(n);
            int[] n1 = new int[a4 / 2];
            int[] n2 = new int[a4 / 2];
            int number1 = 0;
            int number2 = 0;
            for(int i = 0; i < n.Length; i++)
            {
                if(n[i] % 2 == 0)
                {
                    number1 = number1 + n[i];
                }
                else
                {
                    number2 = number2 + n[i];
                }
            }
            if(number1 > number2)
            {
                Console.WriteLine("На Улице с четныии домами");
            }
            else
            {
                Console.WriteLine("На улице с нечёными домами");
            }
        }
           
    }
}
