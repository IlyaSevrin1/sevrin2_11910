﻿using System;

namespace _4._2
{
    class Program
    {
        static double func3(double x, double amet)
        {
            if (x < amet)
            {
                return 0;
            }
            else if (x > amet)
            {
                return (x - amet) / (x + amet);
            }
            else
            {
                return 1;
            }
        }
        static double func2(double x)
        {
            if ((Math.Abs(x) < 9) && (Math.Abs(x) >= 3))
            {
                return Math.Sqrt(Math.Pow(x, 2) + 1) / Math.Sqrt(Math.Pow(x, 2) + 5);
            }
            else if (Math.Abs(x) >= 9) 
            {
                return Math.Sqrt(Math.Pow(x, 2) + 1) - Math.Sqrt(Math.Pow(x, 2) + 5);
            }
            else 
            {
                return Math.Sin(x);
            }
        }
        static double func1(double x)
        {
            if (x >= 0.0)
            {
                return 1 / (0.1 + Math.Pow(x, 2));
            }
            else if ((x >= 0.0) && (x<=0.9))
            {
                return (0.2 * x + 0.1);
            }
            else
            {
                return Math.Pow(x, 2) + 0.2;
            }
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Задание 1"); // Задание 1
            Console.WriteLine("Нажмите любую кнопку");
            Console.ReadKey();
            Console.WriteLine("Введите число а");
            double a = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите число в");
            double b = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите шаг h");
            double h = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine();
            Console.Write('x' + "      " + 'f');
            Console.WriteLine();
            Console.WriteLine();
            for(double i = a; i < b; i = i + h)
            {
                Console.Write(i + "     ");
                Console.WriteLine(func1(i));
            }
            Console.WriteLine("Задание 2"); // Задание 2
            Console.WriteLine("Нажмите любую кнопку");
            Console.ReadKey();
            Console.WriteLine("Введите число а");
            double a2 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите число в");
            double b2 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите шаг h");
            double h2 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine();
            Console.Write('x' + "      " + 'f');
            Console.WriteLine();
            Console.WriteLine();
            for (double i2 = a2; i2 < b2; i2 = i2 + h)
            {
                Console.Write(i2 + "     ");
                Console.WriteLine(func2(i2));
            }
            Console.WriteLine();
            Console.WriteLine("Задание 3"); // Задание 3
            Console.WriteLine("Нажмите любую кнопку");
            Console.ReadKey();
            Console.WriteLine("Введите число а");
            double a3 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите число в");
            double b3 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите шаг h");
            double h3 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine();
            Console.Write('x' + "      " + 'f');
            Console.WriteLine();
            Console.WriteLine();
            double amet = a3;
            for (double i3 = a3; i3 < b3; i3 = i3 + h3)
            {
                Console.Write(i3 + "     ");
                Console.WriteLine(func3(i3, amet));
            }
        }
    }
}
