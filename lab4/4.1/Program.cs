﻿using System;

namespace _1._1
{
    class Program
    {
        static double min(double a, double b) // Метод задание 1
        {
            return (a < b) ? a : b;
        }
        static double func2(double x) // Метод задание 2
        {
            return Math.Pow(x, 3) * (-Math.Sin(x));
        }
        static double func3(double x) // Метод задание 3
        {
            return x / 100 % 10; 
        }
        static double func4(double n) // Метод задание 4
        {
            return Math.Sqrt(n) + n;
        }
        static double func5(double x) // Метод задание 5
        {
            return (x % 2 == 0) ? x / 2 : 0;
        }
        static double func6(double x) // Метод задание 6
        {
            return (x % 5 == 0) ? x / 5 : x - 1;
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Задание 1"); // Задание 1
            Console.WriteLine("Нажмите любую кнопку");
            Console.ReadKey();
            Console.WriteLine("Введите первое число");
            double x = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите второе число");
            double y = Convert.ToDouble(Console.ReadLine());
            double z = min(3 * x, 2 * y) + min(x - y, x + y);
            Console.WriteLine("Z = " + z);
            Console.WriteLine();
            Console.WriteLine("Задание 2"); // Задание 2
            Console.WriteLine("Нажмите любую кнопку");
            Console.ReadKey();
            Console.WriteLine("Введите число a");
            double a = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите число в");
            double b = Convert.ToDouble(Console.ReadLine());
            if (func2(a) < func2(b))
            {
                Console.WriteLine("Наибольшее значение функции в точке в");
            }
            else if (func2(a) > func2(b))
            {
                Console.WriteLine("Наибольшее значение функции в точке а");
            }
            else
            {
                Console.WriteLine("Точки совпадают");
            }
            Console.WriteLine();
            Console.WriteLine("Задание 3"); // Задание 3
            Console.WriteLine("Нажмите любую кнопку");
            Console.ReadKey();
            Console.WriteLine("Введите первое число:");
            double a1 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите второе число:");
            double b1 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите третье число");
            double c1 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine(func3(a1) + func3(b1) + func3(c1));
            Console.WriteLine();
            Console.WriteLine("Задание 4"); // Задание 4
            Console.WriteLine("Нажмите любую кнопку");
            Console.ReadKey();
            Console.WriteLine(func4(6) / 2 + func4(13) / 2 + func4(21) / 2);
            Console.WriteLine();
            Console.WriteLine("Задание 5"); // Задание 5
            Console.WriteLine("Нажмите любую кнопку");
            Console.ReadKey();
            Console.WriteLine("Введите число");
            double a5 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine(func5(a5));
            Console.WriteLine();
            Console.WriteLine("Задание 6"); // Задание 6
            Console.WriteLine("Нажмите любую кнопку");
            Console.WriteLine("Введите число");
            double a6 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine(func6(a6));
            Console.ReadKey();
        }
    }
}
